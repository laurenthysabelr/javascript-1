// STRING 
var persona = "Fidel";
let parentesco = "abuelo";

//NUMBER 
var cumpleaños = 10;
let despedida = 26;

//BOOLEAN
var eraespecialparami = true;
let estavivo = false;

//UNDEFINED
var diadeencontrarnos = undefined;

//NULL
var salirlosdomingos = null;

//ARRAY 
let comoera = ["sabio", "prudente", "admirable", "genial","true"];

//OBJECT
var gaveta = {
    color: "Marrón",
    acceso: "Limitado", 
    contenido: "Esperanza", 
}
